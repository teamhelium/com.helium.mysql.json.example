<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JsonExample extends Model
{

    public $fillable = ['type', 'data'];

    public $casts = ['data' => 'array'];

    public static function test(){

        $house = ['bedrooms' => 25, 'owner' => 'John'];
        $office = ['offices' => 22, 'company' => 'Helium'];

        //Valid Insert
        $houseArray = ['type' => 'house', 'data' => $house];
        $officeArray = ['type' => 'office', 'data' => $office];
        self::run('Valid Insert', $houseArray, $officeArray);

        //Swap House and Office
        $houseArray = ['type' => 'house', 'data' => $office];
        $officeArray = ['type' => 'office', 'data' => $house];
        self::run('Invalid Insert - Swap House And Office', $houseArray, $officeArray);

        //Invalid Values
        $house = ['bedrooms' => 50, 'owner' => 'John'];
        $office = ['offices' => 45, 'company' => 'Helium'];
        $houseArray = ['type' => 'house', 'data' => $house];
        $officeArray = ['type' => 'office', 'data' => $office];
        self::run('Invalid Insert - Invalid Values House and Office (Outside Range)', $houseArray, $officeArray);

        //Invalid Field Names
        $house = ['bedroo' => 24, 'owner' => 'John'];
        $office = ['offic' => 23, 'company' => 'Helium'];
        $houseArray = ['type' => 'house', 'data' => $house];
        $officeArray = ['type' => 'office', 'data' => $office];
        self::run('Invalid Insert - Invalid Field Names', $houseArray, $officeArray);

    }

    public static function accessData(){

        echo "Access Data". PHP_EOL;

        $data = self::all();

        foreach ($data as $randomItem){

            if($randomItem->type == 'house'){
                echo 'Accessing Data (House - Owner): using - $randomItem->data[\'owner\']';
                echo PHP_EOL.PHP_EOL;
                echo $randomItem->data['owner'];
                echo PHP_EOL.PHP_EOL;

                echo 'Accessing Data (House - Bedrooms): using - $randomItem->data[\'bedrooms\']';
                echo PHP_EOL.PHP_EOL;
                echo $randomItem->data['bedrooms'];
                echo PHP_EOL.PHP_EOL;
            }

            if($randomItem->type == 'office'){
                echo 'Accessing Data (House - Offices): using - $randomItem->data[\'offices\']';
                echo PHP_EOL.PHP_EOL;
                echo $randomItem->data['offices'] ;
                echo PHP_EOL.PHP_EOL;

                echo 'Accessing Data (House - Company): using -  $randomItem->data[\'company\']';
                echo PHP_EOL.PHP_EOL;
                echo $randomItem->data['company'];
                echo PHP_EOL.PHP_EOL;
            }
        }


        echo "Querying the DB. Here we can use where clauses just like normal, except to query our JSON we use the arrow operator:" . PHP_EOL.PHP_EOL;
        echo "Using: JsonExample::where('type', 'house')->where('data->bedrooms', 22)->get()". PHP_EOL.PHP_EOL;

        $results = JsonExample::where('type', 'house')->where('data->bedrooms', 22);
        echo "Query: " . PHP_EOL . $results->toSql() . PHP_EOL.PHP_EOL;
        print_r($results->get()->toArray());

        //TODO: Add Updating Example.


    }

    private static function run($header, $houseArray, $officeArray){

        echo PHP_EOL. $header . PHP_EOL;

        print_r($houseArray);
        print_r($officeArray);

        try{
            $houseStatus = !is_null(self::create($houseArray)) ? 'House Created' : 'House Not Created';
            echo $houseStatus . PHP_EOL.PHP_EOL;;


        }
        catch (\Exception $e){
            echo "Error: " . $e->getMessage() . PHP_EOL.PHP_EOL;;
        }

        try{
            $officeStatus = !is_null(self::create($officeArray)) ? 'Office Created' : 'Office Not Created';
            echo $officeStatus . PHP_EOL.PHP_EOL;;

        }
        catch (\Exception $e){
            echo "Error: " . $e->getMessage() . PHP_EOL.PHP_EOL;;
        }



    }

}
