<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJsonExamplesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $typeContraint =
            'CONSTRAINT `constraint_type` 
            CHECK(
                (JSON_SCHEMA_VALID 
                    (\'{"type": "object",
                          "properties": {
                            "bedrooms": {
                              "type": "number",
                              "minimum": 20,
                              "maximum": 25
                            },
                            "owner": {
                              "type": "string"
                            }
                          },
                          "required": ["bedrooms", "owner"]
                        }\', `data`
                    ) 
                    AND `type` = \'house\' 
                ) 
                
                OR
                
                (JSON_SCHEMA_VALID 
                    (\'{"type": "object",
                          "properties": {
                            "offices": {
                              "type": "number",
                              "minimum": 20,
                              "maximum": 25
                            },
                            "company": {
                              "type": "string"
                            }
                          },
                          "required": ["offices", "company"]
                        }\', `data`
                    ) 
                    AND `type` = \'office\' 
                ) 
                
              
            )';

        Schema::create('json_examples', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->enum('type', ['house', 'office']);
            $table->json('data');
        });

        DB::statement("ALTER TABLE json_examples ADD $typeContraint");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('json_examples');
    }
}
