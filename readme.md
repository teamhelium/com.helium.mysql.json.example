<p align="center"><img src="https://heliumservices.com/wp-content/uploads/2017/10/HeliumServices.png" width="200"></p>

#JSON Schema Validation MySQL & Laravel

## Creating Migration

JSON Validation in MySQL is enforced using [Draft 4 of the JSON Schema Specification](https://json-schema.org/specification-links.html#draft-4).
Using this spec, we can check the validity of the JSON, the correctness of type, and set parameters to enforce the validity of values.


### Create your Constraint

In the example below bedrooms, and owner are being checked for type, the `bedroom` attribute with is also being checked for a range below.

After each `JSON_SCHEMA_VALID` function there is an `AND` keyword, which allows us to provide a SQL statement to limit the validity check based on the value of a particular column.

```

$typeContraint =
            'CONSTRAINT `constraint_type` 
            CHECK(
                (JSON_SCHEMA_VALID 
                    (\'{"type": "object",
                          "properties": {
                            "bedrooms": {
                              "type": "number",
                              "minimum": 20,
                              "maximum": 25
                            },
                            "owner": {
                              "type": "string"
                            }
                          },
                          "required": ["bedrooms", "owner"]
                        }\', `data`
                    ) 
                    AND `type` = \'house\' 
                ) 
                
                OR
                
                (JSON_SCHEMA_VALID 
                    (\'{"type": "object",
                          "properties": {
                            "offices": {
                              "type": "number",
                              "minimum": 20,
                              "maximum": 25
                            },
                            "company": {
                              "type": "string"
                            }
                          },
                          "required": ["offices", "company"]
                        }\', `data`
                    ) 
                    AND `type` = \'office\' 
                ) 
                
              
            )';


```

### Build your Migration

```

Schema::create('json_examples', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->enum('type', ['house', 'office']);
            $table->json('data');
});

//You cannot create the table and constraint at the same time due to a limitation with the Schema class.
//However, this contraint can be created by running a query directly against that database as below. 

DB::statement("ALTER TABLE json_examples ADD $typeContraint");


```

This migration can now be ran as normal using `php artisan migrate`

## Model

Our model needs to cast our JSON to an array, this way we can access our data via our normal attribute assessors for our model. 

```
public $casts = ['data' => 'array'];
```

Now when access our data object we should be able to access our data as values within this array.

**Caveat:** Casting to an array means that you can set data as an array and the json is automatically created before insert,
with this option you can only access the json data as a value from the `data` object.

```
public $casts = ['data' => 'array'];

//When setting array must be set as follows:
$house = ['bedrooms' => 25, 'owner' => 'John'];
$houseArray = ['type' => 'house', 'data' => $house];

//Accessing Data
...
$item->data['owner'];

``` 

If you change the cast to an object type, you can access data as a using a typical arrow operator, but in this case you 
must convert json data using `json_decode` before setting the `data` value.

```
public $casts = ['data' => 'object'];

//When setting array must be set as follows:
$house = ['bedrooms' => 25, 'owner' => 'John'];
$houseArray = ['type' => 'house', 'data' => json_encode($house)];

//Accessing Data
...
$item->data->owner;

``` 

**Pick you poison here, not sure it matters much, I like the ideal of not needing to call `json_encode`. 
The array cast seems cleaner me.**

### Querying the database

The JSON object can be queried along with table level data as well.

This is done using our normal Eloquent clauses. 

Data in the JSON can be accessed using an arrow operator: `data->item`, the arrow serves as an alias for the `JSON_EXTRACT` MySQL function.

This example gets results where normal `STRING` field type = 'house' and the `JSON` field data, bedrooms attribute is 25.

```

$results = JsonExample::where('type', 'house')->where('data->bedrooms', 22)->get();


```

## Working Examples

There are two examples contained with in this project:

1. `JsonExample::test()` - Inserts to Database, using object, along with testing different validation states.
2. `JsonExample::accessData()` - Inserts to Database, using object, along with testing different validation states.

```
php artisan tinker
JsonExample::test();
JsonExample::accessData();
```

#### TODO

Add Updating Data Section to JsonExample.

#### Contributors

* Terrence Jackson (terrence@heliumservices.com)